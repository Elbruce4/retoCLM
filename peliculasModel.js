const mongoose = require("mongoose");
const {Schema} = mongoose;
const moongosePaginate = require("mongoose-paginate-v2")

const pelicula = new Schema({
    Title : String,
    Year : String,
    Released : String,
    Genre : String,
    Director : String,
    Actors : Array,
    Plot: String,
    Ratings: Array
})

pelicula.plugin(moongosePaginate)

const Model = mongoose.model("peliculas" , pelicula);
module.exports = Model

/* const Prueba = new Schema({name : String});
const Model2 = mongoose.model("CLM", Prueba);
module.exports = Model2 */