//const express = require("express");
const express = require('express')
const app = express();
const connection = require("./DBConection/connection.js");

app.listen(3000 , async ()=> {
  console.log("escuchando en puerto 3000");
  await connection()
})

module.exports = app