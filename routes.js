const route = require("./index.js");
const modelos = require("./peliculasModel.js");
const axios = require("axios");
var express = require('express')
//api : http://www.omdbapi.com/?i=tt3896198&apikey=7873204c
//apikey: 7873204c

route.use(express.json())

route.get("/film/:filmName" , async (req,res)=> {

    let {filmName} = req.params;
    let {year} = req.query;
    let {pages} = req.headers;

    let UpercaseName = filmName.slice(0,1).toLocaleUpperCase() + filmName.slice(1,filmName.length);
    
    try {

        let isFilm = await modelos.find({
            Title: UpercaseName
        });
        
        if(isFilm.length === 0){
            if(!year){
                if(filmName.split(" ").length === 1){
                    const film = await axios.get(`http://www.omdbapi.com/?t=${filmName}&apikey=7873204c`);
                    await modelos.create({
                        Title: film.data.Title,
                        Year: film.data.Released.split(" ").pop(),
                        Released : film.data.Released,
                        Genre: film.data.Genre,
                        Director: film.data.Director,
                        Actors: film.data.Actors.split(","),
                        Plot: film.data.Plot,
                        Ratings: film.data.Ratings.map(obj => obj.Value)
                    });
                    let filmsDB = await modelos.paginate({},{
                        page : pages,
                        limit: 5
                    });
                    res.send(filmsDB)
                } else if(filmName.split(" ").length > 1) {
                    const newFilmName = filmName.split(" ").join("+");
                    const film = await axios.get(`http://www.omdbapi.com/?t=${newFilmName}&apikey=7873204c`);
                    await modelos.create({
                        Title: film.data.Title,
                        Year: film.data.Released.split(" ").pop(),
                        Released : film.data.Released,
                        Genre: film.data.Genre,
                        Director: film.data.Director,
                        Actors: film.data.Actors.split(","),
                        Plot: film.data.Plot,
                        Ratings: film.data.Ratings.map(obj => obj.Value)
                    });
                    let filmsDB = await modelos.paginate({},{
                        page : pages,
                        limit: 5
                    });
                    res.send(filmsDB)
                }
            } else {
                const film = await axios.get(`http://www.omdbapi.com/?t=${filmName}&y=${year}&apikey=7873204c`);
                await modelos.create({
                    Title: film.data.Title,
                    Year: year,
                    Released : film.data.Released,
                    Genre: film.data.Genre,
                    Director: film.data.Director,
                    Actors: film.data.Actors.split(","),
                    Plot: film.data.Plot,
                    Ratings: film.data.Ratings.map(obj => obj.Value)
                });
                let filmsDB = await modelos.paginate({},{
                    page : pages,
                    limit: 5
                });
                res.send(filmsDB)
            }
        } else {
            let filmsDB = await modelos.paginate({},{
                page : pages,
                limit: 5
            });
            res.send(filmsDB)
        }
    } catch (error) {
        res.status(404).send(error)
    }
})

route.post("/" , async(req,res) => {
    let {movie , find , replace} = req.body
    try {
        let nameMovie = movie.slice(0,1).toLocaleUpperCase() + movie.slice(1,movie.length);
        let isFilm = await modelos.find({
            Title: nameMovie
        });
        let newPlot = isFilm[0].Plot.split(" ")
        .map(obj => {
            if(obj === find){
               return obj = replace;
            } else {
                return obj
            }
        })
        newPlot = newPlot.join(" ");
        isFilm[0].Plot = newPlot
        console.log(isFilm)
        await isFilm[0].save()
        res.send(newPlot)
    } catch (error) {
        res.send(error).status(404)
    }
})

